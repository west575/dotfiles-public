
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(dired-recursive-copies (quote always))
 '(ledger-reports
   (quote
    (("equity" "ledger ")
     ("bal" "%(binary) -f %(ledger-file) bal")
     ("reg" "%(binary) -f %(ledger-file) reg")
     ("payee" "%(binary) -f %(ledger-file) reg @%(payee)")
     ("account" "%(binary) -f %(ledger-file) reg %(account)"))))
 '(org-archive-subtree-save-file-p t)
 '(org-clock-ask-before-exiting nil)
 '(org-clock-persist (quote history))
 '(org-clock-persistence-insinuate nil)
 '(org-list-demote-modify-bullet (quote (("+" . "-") ("-" . "+") ("*" . "+"))))
 '(org-modules (org-habit org-checklist))
 '(org-odt-preferred-output-format "docx")
 '(org-trello-current-prefix-keybinding "C-c o")
 '(package-archive-priorities (quote (("melpa-stable" . 10) ("gnu" . 10))))
 '(package-selected-packages
   (quote
    (wc-goal-mode slime org-contrib magit ox-reveal esxml salt-mode golint json-mode org-trello gnu-elpa-keyring-update csv-mode go-mode visual-fill-column dockerfile-mode markdown-mode auctex auto-package-update org-jira ledger-mode terraform-mode nov unfill org-plus-contrib exec-path-from-shell use-package-ensure-system-package use-package)))
 '(safe-local-variable-values
   (quote
    ((eval lexical-let
           ((project-directory
             (car
              (dir-locals-find-file default-directory))))
           (set
            (make-local-variable
             (quote flycheck-javascript-eslint-executable))
            (concat project-directory ".yarn/sdks/eslint/bin/eslint.js"))
           (eval-after-load
               (quote lsp-javascript)
             (quote
              (progn
                (plist-put lsp-deps-providers :local
                           (list :path
                                 (lambda
                                   (path)
                                   (concat project-directory ".yarn/sdks/" path))))
                (lsp-dependency
                 (quote typescript)
                 (quote
                  (:local "typescript/bin/tsserver")))))))
     (lsp-enabled-clients ts-ls eslint))))
 '(sort-fold-case t))
