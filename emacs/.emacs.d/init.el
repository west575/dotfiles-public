; initialize speechd-el first since it is critical

(setq speechd-out-active-drivers (quote (ssip)))
(setq speechd-speak-buffer-name t)
(setq speechd-speak-deleted-char t)
(setq speechd-speak-read-command-keys (quote (modification modification-movement)))
(setq speechd-speak-whole-line nil)

(condition-case err
(progn
(autoload 'speechd-speak "speechd-speak" nil t)
(speechd-speak)
(speechd-speak-set-language "en-US")
(speechd-set-rate 100 t)
(speechd-set-punctuation-mode `all t)
(speechd-set-volume 25 t)
)
(error
 (message "Cannot load speechd-el %s" (cdr err))))

;; Update package-archive lists
(require 'package)
(setq package-enable-at-startup nil)

(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives '("org-contrib" . "https://elpa.nongnu.org/nongnu/") t)
(custom-set-variables
 '(package-archive-priorities '(
                                ("melpa-stable" . 10)
                                ("gnu" . 10)
                                )
                              )
 )

(package-initialize)
(setq use-package-verbose t)

;; Install 'use-package' if necessary
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Enable use-package
(eval-when-compile
  (require 'use-package))

; use-package features for later use

(use-package use-package-ensure-system-package
  :ensure t)

; use-package declarations for required packages

(use-package exec-path-from-shell
  :demand
  :ensure t
  :pin melpa-stable
  :custom
  (exec-path-from-shell-check-startup-files nil)
  :config
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize)))
(exec-path-from-shell-copy-env "GOPATH")

(use-package gnu-elpa-keyring-update
  :demand
  :ensure t
  :pin gnu)

(use-package org
  :ensure t
  :pin gnu
  :custom
( org-stuck-projects
       '("+LEVEL=2/-DONE"
     ("TODO" "NEXT" "HOLD")
     nil
     "SCHEDULED:\\|DEADLINE:"))
  (org-modules '(org-habit org-checklist))
  (org-clock-persist 'history)
  (org-clock-ask-before-exiting nil)
  (org-odt-preferred-output-format "docx")
  (org-archive-subtree-save-file-p t)
       (org-clock-persistence-insinuate))

(use-package org-contrib
  :ensure t
  :pin org-contrib)

(use-package unfill
  :ensure t
  :pin melpa-stable
  :bind ([remap fill-paragraph] . unfill-toggle))

(use-package nov
  :ensure t
  :pin melpa-stable
  :ensure-system-package unzip
  :mode ("\\.epub\\'" . nov-mode))

(use-package hcl-mode
  :ensure t
  :pin melpa-stable)

(use-package terraform-mode
  :after hcl-mode
  :ensure t
  :pin melpa-stable
  :mode "\\.\\(tf\\(vars\\)?\\)\\|\\(hcl\\)\\'"
  :hook (terraform-mode . terraform-format-on-save-mode)
  :custom
  (terraform-indent-level 2))

(use-package ledger-mode
  :pin melpa-stable
  :ensure t
  :ensure-system-package ledger
  :mode "\\.ledger$"
  :custom
  (ledger-schedule-file "~/opt/src/ledger/schedule.ledger")
  (ledger-reconcile-finish-force-quit t))

(use-package magit
  :ensure t
  :pin melpa-stable
  :bind ("C-x g" . magit-status))

(use-package org-jira
  :pin melpa-stable
  :ensure t
  :custom
  (jiralib-url "https://rentpath.atlassian.net")
  (org-jira-working-dir "~/.org-jira")
  :bind
  ("C-c pg" . 'org-jira-get-projects))

(use-package auto-package-update
  :ensure t
  :pin melpa-stable
  :custom
 (auto-package-update-excluded-packages '(org))
  (auto-package-update-delete-old-versions t)
  (auto-package-update-hide-results nil)
  :config
  (auto-package-update-maybe))


(use-package tex
  :ensure auctex
  :pin melpa-stable
  :config
  (setq TeX-auto-save t)
     (setq TeX-parse-self t)
     (setq-default TeX-master nil)  )

(use-package markdown-mode
  :ensure t
  :pin melpa-stable
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :custom (markdown-command "pandoc"))

(use-package dockerfile-mode
  :ensure t
  :pin melpa-stable
  :mode ("Dockerfile\\'" . dockerfile-mode))

(use-package go-mode
  :ensure t
  :pin melpa-stable
  :mode ("\\.go\\'" . go-mode)
  :hook (before-save . gofmt-before-save))

(eval-and-compile
(defun golint-load-path ()
  (concat (getenv "GOPATH")  "/src/golang.org/x/lint/misc/emacs")))

(use-package golint
:pin manual
:after go-mode
:load-path (lambda () (list (golint-load-path))))

(use-package csv-mode
  :ensure t
  :pin gnu
  :mode ("\\.[Cc][Ss][Vv]\\'" . csv-mode))


(use-package json-snatcher
  :ensure t
  :pin melpa-stable
    :mode ("\\.releaserc\\'" . json-mode)
  )


(use-package json-reformat
  :ensure t
  :pin melpa-stable)

(use-package json-mode
  :ensure t
  :after (json-snatcher json-reformat)
  :pin melpa-stable
:custom   (js-indent-level 2))

(use-package yaml-mode
  :ensure t
  :pin melpa-stable
  :mode   "\\.yml\\'"
  :bind ("C-m" . newline-and-indent))
(use-package ox-reveal
:ensure t
  :pin melpa)

(use-package slime
  :ensure t
  :pin melpa-stable
  :custom
  (inferior-lisp-program "/usr/bin/sbcl")
  )

(use-package wc-goal-mode
  :ensure t
  :pin melpa-stable
:bind ("C-c w" . wc-goal-mode)  )

(add-hook 'comint-output-filter-functions 'comint-strip-ctrl-m)
(add-hook 'text-mode-hook 'text-mode-hook-identify)

(set-fill-column 72)

; org mode key bindings
     (global-set-key "\C-cl" 'org-store-link)
     (global-set-key "\C-cc" 'org-capture)
     (global-set-key "\C-ca" 'org-agenda)
     (global-set-key "\C-cb" 'org-iswitchb)

; org mode setup
(custom-set-variables '(org-modules (org-habit org-checklist))
  '(org-clock-persist 'history)
  '(org-clock-ask-before-exiting nil)
  '(org-odt-preferred-output-format "docx")
  '(org-archive-subtree-save-file-p t)
       '(org-clock-persistence-insinuate))

(setq org-agenda-files (quote ("~/opt/src/gtd/")))
(setq org-agenda-skip-scheduled-if-deadline-is-shown (quote repeated-after-deadline))
(setq org-agenda-span (quote fortnight))
(setq org-agenda-start-on-weekday nil)
(setq org-agenda-todo-ignore-deadlines (quote near))
(setq org-agenda-todo-ignore-scheduled (quote all))
(setq org-agenda-todo-list-sublevels nil)
(setq org-catch-invisible-edits (quote smart))
(setq org-directory "~/opt/src/gtd")
(setq org-default-notes-file (concat org-directory "/notes.org"))
(setq org-enforce-todo-dependencies t)
(setq org-log-done (quote time))
(setq org-refile-allow-creating-parent-nodes (quote confirm))
(setq org-refile-targets (quote ((org-agenda-files :maxlevel . 2))))
(setq org-refile-use-outline-path (quote file))
(setq org-startup-folded nil)
(setq org-todo-keywords
   (quote
    ((sequence "TODO(t)" "NEXT(n)" "DONE(d)")
     (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELED" "PHONE" "MEETING"))))
(setq org-treat-S-cursor-todo-selection-as-state-change nil)
(setq org-use-speed-commands t)
(setq org-time-stamp-rounding-minutes (quote (1 1)))
(setq org-agenda-clock-consistency-checks
      (quote (:max-duration 1
              :min-duration 0
              :max-gap 0
              :gap-ok-around 1)))
(setq org-clock-out-remove-zero-time-clocks t)
(setq org-agenda-clockreport-parameter-plist
      (quote (:link t :maxlevel 5 :fileskip0 t :compact t :narrow 80)))
(setq org-columns-default-format "%80ITEM(Task) %10Effort(Effort){:} %10CLOCKSUM")
(setq org-agenda-compact-blocks t)
(setq org-global-properties (quote (("Effort_ALL" . "0:15 0:30 1:00 2:00 3:00 4:00 5:00 6:00 7:00 8:00")
                                    ("STYLE_ALL" . "habit"))))
(setq org-agenda-log-mode-items (quote (closed state)))
(setq org-fast-tag-selection-single-key (quote expert))
(setq org-agenda-tags-todo-honor-ignore-options t)
(setq org-archive-mark-done nil)
(setq org-agenda-sticky nil)

; cycle-spacing introduced in emacs 24.4.
(global-set-key "\M-SPC" 'cycle-spacing)

                                        ; the new indentation engine for shell mode plays havok with speechd-el
(setq sh-use-smie nil)

                                        ; turn off character composition
(global-auto-composition-mode -1)

;; Mutt support.

(setq auto-mode-alist (append '(("/tmp/mutt.*" . mail-mode)) auto-mode-alist))

     (setq calendar-latitude 33.9)
     (setq calendar-longitude -84.3)
     (setq calendar-location-name "Duluth, GA")

(setq TeX-PDF-mode t)
(setq column-number-mode t)
(setq comment-multi-line t)
(setq compilation-ask-about-save nil)
(setq delete-active-region (quote kill))
(setq dired-recursive-deletes (quote always))
(setq holiday-bahai-holidays nil)
(setq-default indent-tabs-mode nil)
(setq inhibit-startup-screen t)
(setq initial-buffer-choice t)
(setq initial-scratch-message nil)
(setq kill-do-not-save-duplicates t)
(setq kill-read-only-ok t)
(setq line-move-visual nil)
(setq line-number-mode t)
(setq load-prefer-newer t)
(setq next-screen-context-lines 0)
(setq python-remove-cwd-from-path nil)
(setq scroll-error-top-bottom t)
(setq tab-width 4)
(setq uniquify-buffer-name-style (quote post-forward))
(setq uniquify-strip-common-suffix t)
(setq view-read-only t)

(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(server-start)
     (desktop-save-mode 1)

(setq save-place-file "~/.emacs.d/saveplace")
(setq-default save-place t)
(require 'saveplace)

(add-hook 'before-save-hook '(lambda()
                              (when (not (or (derived-mode-p 'markdown-mode)))
                                (delete-trailing-whitespace))))

;;; ORG-MODE:  * My Task
;;;              SCHEDULED: <%%(diary-last-day-of-month date)>
;;; DIARY:  %%(diary-last-day-of-month date) Last Day of the Month
;;; See also:  (setq org-agenda-include-diary t)
;;; (diary-last-day-of-month '(2 28 2017))
(defun diary-last-day-of-month (date)
"Return `t` if DATE is the last day of the month."
  (let* ((day (calendar-extract-day date))
         (month (calendar-extract-month date))
         (year (calendar-extract-year date))
         (last-day-of-month
            (calendar-last-day-of-month month year)))
    (= day last-day-of-month)))

(setq custom-file "~/.emacs.d/custom.el")
     (load custom-file)
